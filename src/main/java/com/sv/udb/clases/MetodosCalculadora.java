/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sv.udb.clases;

import javax.swing.JOptionPane;

/**
 *
 * @author rigoberto
 */
public class MetodosCalculadora {

    public MetodosCalculadora() {
    }
    
   
    /**
     *
    */
    public double sumar(double num1,double num2){
        double resp = 0;
        
        resp = num1 + num2;
        
        JOptionPane.showMessageDialog(null, resp);
        return resp;
    }
    
    public double resta(double num1, double num2){
        double resp = 0;
        
        resp = num1 - num2;
        JOptionPane.showMessageDialog(null, resp);
        return resp;
    }
    
    public double multiplicacion(double num1, double num2){
        double resp = 0;
        
        resp = num1 * num2;
        JOptionPane.showMessageDialog(null, resp);
        return resp;
    }
    
    public double division(double num1, double num2) {
    
        double resp = 0;
        
        resp = num1 / num2;
        JOptionPane.showMessageDialog(null, resp);
        return resp;
    
    }
    
}
