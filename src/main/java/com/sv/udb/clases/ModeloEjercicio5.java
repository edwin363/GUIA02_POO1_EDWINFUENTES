/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sv.udb.clases;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import java.io.*;

/**
 *
 * @author rigoberto
 */
public class ModeloEjercicio5 {

    FileInputStream entrada;
    BufferedWriter salida;
    FileOutputStream salida2;

    public ModeloEjercicio5() {
    }

    public void CrearArchivo() {
        JFileChooser saveAs = new JFileChooser();
        saveAs.setApproveButtonText("Guardar");
        saveAs.showSaveDialog(null);
        File archive = new File(saveAs.getSelectedFile() + ".txt");
        try {
            BufferedWriter salida = new BufferedWriter(new FileWriter(archive));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error" + JOptionPane.ERROR_MESSAGE);
        }
    }

    public String GuardarTexto(String texto, File url) {
        /*File f;
        FileWriter w;
        BufferedWriter bw;
        PrintWriter wr;*/
        String respuesta = "";
        try {
            salida2 = new FileOutputStream(url);
            byte[] bytestxt = texto.getBytes();
            salida2.write(bytestxt);
            JOptionPane.showMessageDialog(null, "Guardado correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error" + e);
        }
        return respuesta;
    }
    
    public String MostratTexto(File archivo){
        String contenido = "";
        
        try {
            entrada = new FileInputStream(archivo);
            int ascci;
            while((ascci = entrada.read()) != -1){
                char caracter = (char)ascci;
                contenido += caracter;
            }
        } catch (Exception e) {
        }
        
        return contenido;
    }
    
    public void EliminarFichero(File url){
        if(!url.exists()){
            JOptionPane.showMessageDialog(null, "El archivo no existe");
        }else{
            url.delete();
            JOptionPane.showMessageDialog(null, "Archivo borrado correctamente");
        }
    }

}
